<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Questionario sulla conoscenza di Prodotto - login</title>

    <!-- CSS -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="page-header">
                  <h1>Loggati al sistema</h1>
                  <h3>Rispondi alle domande o accedi all'area di amministrazione</h3>
              </div>
          </div>
          <div class="row">
              <div class="panel panel-info">
                  <div class="panel panel-heading">
                      <h1 class="panel-title">Dati di accesso</h1>
                  </div>
                  <div class="panel panel-body">
                      <div class="form-group">
                          <label for="mail">Email</label>
                          <input type="email" id="mail" name="mail" class="form-control" placeholder="Email..."/>
                      </div>
                      <div class="form-group">
                          <label for="password">Password</label>
                          <input type="password" id="password" name='password' class='form-control' placeholder="password..."/>
                      </div>
                  </div>
                  <div class='panel panel-footer'>
                      <button id='btnLogin' class='btn btn-info'>Login</button>
                  </div>
              </div>
          </div>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/login.js"></script>
  </body>
</html>