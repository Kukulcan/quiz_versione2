-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: quiz_rifatto
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `domande`
--

DROP TABLE IF EXISTS `domande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domande` (
  `dom_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificatore tabella domande',
  `dom_ute_id` int(11) NOT NULL COMMENT 'ID utente che ha creato la domanda',
  `dom_testo` longtext NOT NULL COMMENT 'Testo della domanda',
  `dom_risposta1` longtext NOT NULL COMMENT 'Testo risposta 1',
  `dom_risposta2` longtext NOT NULL COMMENT 'Testo risposta 2',
  `dom_risposta3` longtext NOT NULL COMMENT 'testo risposta 3',
  `dom_rispostagiusta` int(11) NOT NULL COMMENT 'identificativo risposta corretta',
  `dom_ordine` int(11) NOT NULL COMMENT 'cardinalità ordine delle domande',
  PRIMARY KEY (`dom_id`),
  KEY `dom_ute_idx` (`dom_ute_id`),
  CONSTRAINT `dom_ute` FOREIGN KEY (`dom_ute_id`) REFERENCES `utenti` (`ute_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domande`
--

LOCK TABLES `domande` WRITE;
/*!40000 ALTER TABLE `domande` DISABLE KEYS */;
INSERT INTO `domande` VALUES (1,1,'Di che colore è l\'hud della applicazione?','blu','verde','rosso',1,4),(2,1,'Quanti pulsanti ci sono nel footer?','3','5','8',1,1),(3,4,'Quante voci di menu sono presenti?','10','15','7',3,2),(4,4,'Sono presenti finestre modali?','No','Si','Non lo so',2,5),(5,1,'Per quale sistema operativo è funzionante l\'applicazione?','Android','Windows Phone','IOS',1,3);
/*!40000 ALTER TABLE `domande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranking`
--

DROP TABLE IF EXISTS `ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking` (
  `ran_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID univoco dei ranking',
  `ran_dom_id` int(11) NOT NULL COMMENT 'chiave esterna : collega alla tabella domande',
  `ran_ute_id` int(11) NOT NULL COMMENT 'chiave esterna : collega alla tabella utenti',
  `ran_punti` int(11) NOT NULL COMMENT 'punteggio dato alla risposta : 1 se giusta , -1 se errata',
  `ran_risposta` varchar(45) NOT NULL COMMENT 'identificativo della risposta data : 1 - 2 - 3',
  PRIMARY KEY (`ran_id`),
  KEY `ran_ute_idx` (`ran_ute_id`),
  KEY `ran_dom_idx` (`ran_dom_id`),
  CONSTRAINT `ran_dom` FOREIGN KEY (`ran_dom_id`) REFERENCES `domande` (`dom_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ran_ute` FOREIGN KEY (`ran_ute_id`) REFERENCES `utenti` (`ute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='tabella in cui ogni riga è una risposta data da un navigatore';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranking`
--

LOCK TABLES `ranking` WRITE;
/*!40000 ALTER TABLE `ranking` DISABLE KEYS */;
INSERT INTO `ranking` VALUES (1,2,2,1,'1'),(2,3,2,1,'3'),(3,5,2,1,'1'),(4,1,2,1,'1'),(5,4,2,-1,'1'),(6,2,3,-1,'2'),(7,3,3,-1,'1'),(8,5,3,-1,'2'),(9,1,3,-1,'3'),(10,4,3,1,'2');
/*!40000 ALTER TABLE `ranking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utenti`
--

DROP TABLE IF EXISTS `utenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utenti` (
  `ute_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID degli utenti',
  `ute_mail` varchar(45) NOT NULL COMMENT 'identificativo univoco per gli utenti',
  `ute_password` varchar(45) NOT NULL COMMENT 'password degli utenti',
  `ute_nome` varchar(50) NOT NULL COMMENT 'nome e cognome dell''utente',
  `ute_ruolo` tinyint(1) NOT NULL COMMENT 'Codice per identificare gli utenti\n0 : amministratore\n1 : navigatore\\utente standard',
  PRIMARY KEY (`ute_id`),
  UNIQUE KEY `ute_mail_UNIQUE` (`ute_mail`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utenti`
--

LOCK TABLES `utenti` WRITE;
/*!40000 ALTER TABLE `utenti` DISABLE KEYS */;
INSERT INTO `utenti` VALUES (1,'simone@gmail.com','simone','Simone Cavalli',0),(2,'lucia@gmail.com','lucia','Lucia Lucarelli',1),(3,'marco@gmail.com','marco','Marco Bizzocchi',1),(4,'claudio@gmail.com','claudio','Claudio Rossi',0);
/*!40000 ALTER TABLE `utenti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-06 19:46:27
