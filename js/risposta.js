//funziona associata al click del pulsante rispondi ; effettua una chiamata in ajax a risposta.php

//se la chiamata ha successo ricarica la pagina ( con la domanda seguente in ordine )
function funzioneRisposta(identificatore){
    
    var args={
      risposta:$("#risposta:checked").val(),
      id:identificatore
    };
    
    $.ajax("inc/risposta.php",{
       method:"POST",
       data:args,
       dataType:"json",
       success:function(risposta){
           console.log(risposta);
           if(risposta == "ok"){
             window.location="domanda.php";
         }
       }
    });
}


