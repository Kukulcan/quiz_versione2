//comportamento associato al clic del pulsante di login

/*
 * seleziono la mail e la password inserite e le pass ad una pagina php
 * 
 * questa pagina restitusce delle stringhe diverse in base ad elaborazioni interne
 * 
 * in base alla stringa restituira, redireziona verso pagine diverse
 * 
 * In sintesi, distingue se l'utente con password e mail inserite non esiste/esiste. Se esiste distingue anche
 * se è un navigatore che ha già completato il questionario o un'amministratore
 * 
 * 06/06/2016 -- PAGINA admin/index.php DA COMPLETARE ( backoffice )
 */
$("#btnLogin").click(function(){
   var args = {
     mail: $("#mail").val(),
     password:$("#password").val()
   };
   
   $.ajax("inc/login_function.php",{
      method:"POST",
      data:args,
      dataType:"json",
      success:function(risposta){          
          if(risposta == "amministratore"){
              window.location = "admin/index.php";
          }
          if(risposta == "navigatore"){
              window.location = "domanda.php";
          }
          if( risposta == "presente"){
              window.location="fine_questionario.php";
          }
          if(risposta == "nessuno"){
              $(".page-header").append("<h4 class='text-danger'>Login errato,ritenta</h4>");
          }
      }
   });
});

