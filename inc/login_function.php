<?php
//pagina PHP chiamata in ajax dalla pagina index.

//resituisce una stringa codificata json.
    session_start();
    include("Database.php");
    
    $risposta = "";
    $dbo= new Database();
    //query per selezionare un utente se esiste nel database
    $sql="SELECT * FROM utenti WHERE ute_mail=:mail AND ute_password=:password";
    $dbo->query($sql);
    
    $dbo->bind(":mail",$_POST["mail"]);
    $dbo->bind(":password",$_POST["password"]);
    
    $utente = $dbo->single();
    
    //controllo se esiste l'utente
    if($utente){
        //se l'utente esiste
        
        $_SESSION["ute_id"] = $utente["ute_id"];
        $_SESSION["ute_ruolo"] = $utente["ute_ruolo"];
        
        if($utente["ute_ruolo"] == 0){
            //se l'utente è un amministratore
            $risposta = "amministratore";
        }else if($utente["ute_ruolo"] == 1){
            //se l'utente esiste ed è navigatore/utente standard
            
            //controllo che l'utente con questo id non abbia già fatto il questionario
            
            //seleziono gli id presenti nella tabella RANKING, che rappresenta le risposte
            $sql = "SELECT DISTINCT ran_ute_id FROM ranking WHERE ran_ute_id = :id_attuale";
            $dbo->query($sql);
            $dbo->bind(":id_attuale",$utente["ute_id"]);
            
            $id_presenti = $dbo->resultset();
            if (!$id_presenti ){
                //l'utente esiste e NON HA mai partecipato al sondaggio
                $_SESSION["contatore"] = 1;
            
                $risposta = "navigatore";
            }
            else{
                //l'utente esiste ma HA GIÁ partecipato al sondaggio; vengono presentate le statistiche.
                $risposta = "presente";
            }
                       
        }
    }else{
        //se l'utente non esiste
        $risposta = "nessuno";
    }
    
    echo json_encode($risposta);
?>

