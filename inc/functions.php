<?php

//file PHP che include varie funzioni per non scriverle nelle pagine e separare il codice.
    include("Database.php");
    
    
//FUNZIONI

//seleziona_domanda_giusta(int contatore) : seleziona 1 singola domanda in base all'ordine e al contatore passato
    function seleziona_domanda_giusta($cont){
       
        $dbo = new Database();
        $sql = "SELECT * FROM domande ORDER BY dom_ordine LIMIT $cont,1";
        
        $dbo->query($sql);
        
        $domanda = $dbo->single();
        if($domanda){
            return $domanda;
        }else{
            return "none";
        }
    }
    
    //conta le risposte giuste per l'utente con id passato
    function risposte_giuste($id){
        
        $dbo = new Database();
        
        $sql = "SELECT COUNT(*) AS conteggio_giuste FROM ranking WHERE ran_ute_id=$id AND ran_punti=1";
        $dbo->query($sql);
        
        return $dbo->single()["conteggio_giuste"];
    }
    
    //conta le risposte sbagliate per l'utente con id passato
    function risposte_errate($id){
        $dbo = new Database();
        
        $sql = "SELECT COUNT(*) AS conteggio_errate FROM ranking WHERE ran_ute_id=$id AND ran_punti=-1";
        $dbo->query($sql);
        
        return $dbo->single()["conteggio_errate"];
    }
    
    //somma i punti ottenuti dall'utente con id passato
    function totale_punti($id){
        $dbo = new Database();
        
        $sql = "SELECT SUM(ran_punti) AS somma_punti FROM ranking WHERE ran_ute_id=$id";
        $dbo->query($sql);
        
        return $dbo->single()["somma_punti"];
    }
?>


