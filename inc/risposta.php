<?php

//pagina chiamata in ajax da domanda.php

/*
 * controlla le risposte passate , inserisce un record nella tabella ranking e aumenta il valore del contatore in sessione di 1 alla fine.
 * 
 */
session_start();


include("Database.php");

$dbo = new Database();
$punti = 0;


//DISTINGUO LA RISPOSTA SE è GIUSTA O SBAGLIATA
$risposta_data = $_POST["risposta"];  //1-2-3
$id_domanda = $_POST["id"]; //l'id della domanda che contiene le risposte, anche quella giusta

$sql = "SELECT dom_rispostagiusta FROM domande WHERE dom_id=$id_domanda";
$dbo->query($sql);

$valore_risposta_giusta = $dbo->single()["dom_rispostagiusta"];

if( $risposta_data == $valore_risposta_giusta ){
    //la risposta è corretta
    $punti = 1;
}else{
    //la risposta è sbagliata
    $punti = -1;
}

//INSERISCO UNA RISPOSTA DENTRO LA TABELLA RANKING

$sql = "INSERT INTO ranking(ran_dom_id,ran_ute_id,ran_punti,ran_risposta) VALUES($id_domanda,:ute_id,$punti,$risposta_data)";
$dbo->query($sql);

$dbo->bind(":ute_id",$_SESSION["ute_id"]);
$dbo->execute();

//aumento il contatore di session
$_SESSION["contatore"]+=1;

echo json_encode("ok");
?>

