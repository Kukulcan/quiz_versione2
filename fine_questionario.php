<?php
    session_start();
    include("inc/functions.php");
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Questionario sulla conoscenza di Prodotto - riepilogo </title>

        <!-- CSS -->
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="page-header">
                    <h1>Questionario finito<small> oppure hai già partecipato al sondaggio</small></h1>
                    <h3>Grazie di aver partecipato</h3>
                </div>
                <div class="row">
                    <p class="text-info">Statistiche relative al tuo questionario</p>
                </div>
                <div class="row">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>Numero risposte giuste</th>
                                <th>Numero risposte sbagliate</th>
                                <th>Totale punteggio realizzato</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo risposte_giuste($_SESSION["ute_id"]); ?></td>
                                <td><?php echo risposte_errate($_SESSION["ute_id"]); ?></td>
                                <td><?php echo totale_punti($_SESSION["ute_id"]); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.3.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>


